package com.eonforge.peregrine.parse;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class XmlTest {  
    private static Logger log = LoggerFactory.getLogger(XmlTest.class);

    @Test
    public void testParseGetAndRender() {
        Exception error = null;
        try {
            Xml xml = Xml.parse("pom.xml");
            xml.set(xml.rootDoc, "grp1","project","dependencies","6","groupId");
            System.out.println(Xml.render(xml));
        } catch (Exception e) { e.printStackTrace(); error = e; }
        Assert.assertNull(error);
    }

    @Test
    public void testParseSetAndRender() {
        Exception error = null;
        try {
            Xml xml = Xml.parse("pom.xml");
            //xml.set(xml.rootDoc,"myvalue","project","dependencies","mytag");
            //xml.set(xml.rootDoc,"grp1","project","1","dependency","groupId");
            xml.set(xml.rootDoc, "grp1", "project", "dependencies", "6", 
                    "dependency", "groupId");
            log.info(Xml.render(xml));
            log.info("build.str = " + Xml.getAsXml(xml.rootDoc, "build"));
            log.info("dependencies = "+Xml.getAsMaps(xml.rootDoc,"dependency"));
            log.info("config = " + Xml.getAsMap(xml.rootDoc, "configuration"));
        } catch (Exception e) { e.printStackTrace(); error = e; }
        Assert.assertNull(error);
    }

}
