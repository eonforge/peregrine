package com.eonforge.peregrine.http;

import com.eonforge.peregrine.Config;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class HClientTest {  
    private static Logger log = LoggerFactory.getLogger(HClientTest.class);

    @Test
    public void testParseGetAndRender() {
        Exception error = null;
        try {
            Config.load("peregrine.properties");
            System.out.println(HClient.post(Config.getProperty(
                "google-syntax-url"), "This is a test string to be analyzed"));
        } catch (Exception e) { e.printStackTrace(); error = e; }
        Assert.assertNull(error);
    }

}
