package com.eonforge.peregrine;

//import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    @Test
    public void testConfig() {
        Exception error = null;
        try {
            Config.load("peregrine.properties");
            System.out.println("pwd = " + Config.getPwd());
        } catch (Exception e) { e.printStackTrace(); error = e; }
        Assert.assertNull(error);
    }
}
