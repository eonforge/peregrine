#!/bin/bash

repofile=repos.json
id=0

CLASSPATH=~/.m2/repository/log4j/log4j/1.2.17/log4j-1.2.17.jar:~/.m2/repository/org/slf4j/slf4j-log4j12/1.7.2/slf4j-log4j12-1.7.2.jar:~/.m2/repository/com/jcraft/jsch/0.1.54/jsch-0.1.54.jar:~/.m2/repository/org/slf4j/slf4j-api/1.7.5/slf4j-api-1.7.5.jar:~/.m2/repository/org/eclipse/jgit/org.eclipse.jgit/4.8.0.201706111038-r/org.eclipse.jgit-4.8.0.201706111038-r.jar:~/.m2/repository/com/googlecode/json-simple/json-simple/1.1.1/json-simple-1.1.1.jar:~/src/peregrine/target/peregrine-1.0-SNAPSHOT.jar

while [ "$id" -lt 2 ]
do
 
  curl "https://api.github.com/search/repositories?q=language:Java" > $repofile

  id=`java -cp $CLASSPATH com.eonforge.peregrine.git.RepoWalker $repofile`

  echo "id = $id"

done
