package com.eonforge.peregrine.http;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.*;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HClient {

    public static int timeout = 300 * 1000;
    private static final Logger log = LoggerFactory.getLogger(HClient.class);

    public HClient() {}

    public static String get(String urlStr, Map<String, Object> params) 
	    throws Exception {
	return request(urlStr, params, "GET", null, null);
    }

    public static String post(String urlStr, String data) throws Exception {
	return post(urlStr, textPayload(data));
    }

    public static String post(String urlStr, Map<String, Object> params)
	    throws Exception {
	return request(urlStr, params, "POST", (String) params.get("type"),
			(byte[]) params.get("bytes"));
    }

    public static String request(String urlStr, Map<String, Object> params, 
        String method, String type, byte[] bytes) throws Exception {
	log.info("request.params = "+merge(copy(params),"document",new HashMap()));
	URL url = new URL(urlStr + ("GET" != method ? "" : 
			(urlStr.contains("?") ? "&" : "?")+toUrl(params)));
	HttpURLConnection con = (HttpURLConnection) url.openConnection();
	con.setRequestMethod(method);

	con.setDoOutput(true);
	con.setRequestProperty("Content-Type", 
			type == null? "application/json": type);
	con.setRequestProperty("User-Agent", "Peregrine");
	if (params != null && params.containsKey("username")) {
	    con.setRequestProperty("Authorization", basicAuth(params));
	    log.info("auth = " + con.getRequestProperty("Authorization") + 
		     ", basic=" + basicAuth(params));
	}
	log.info("headers = " + (con==null? "": con.getRequestProperties()));
	if (urlStr.startsWith("https:"))
	  ((HttpsURLConnection)con).setHostnameVerifier(new HostnameVerifier() {
	    @Override
	    public boolean verify(String hostname, SSLSession session) {
	        return true;
	    }
	});
	con.setConnectTimeout(timeout); // 5 min
	con.setReadTimeout(timeout);
	con.setInstanceFollowRedirects(false);
	if ("POST".equals(method)) {
	    if (bytes == null) toStream(con.getOutputStream(), params);
	    else con.getOutputStream().write(bytes);
	}
	
	log.info("HTTP {} status = {}", method, con.getResponseCode());
	String response = toString(con.getInputStream());
	con.disconnect();
	return response;
    }

    private static String basicAuth(Map<String, Object> params) {
	String auth = params.get("username") + ":" + params.get("password");
	return "Basic " + new String(Base64.getEncoder().
			encode(auth.getBytes()));
    }

    public static String toUrl(Map<String, Object> params) 
        throws IOException, UnsupportedEncodingException {
	if (params == null) return "";
	StringBuilder sb = new StringBuilder();
	for (Map.Entry<String, Object> entry : params.entrySet()) {
	    sb.append(sb.length() == 0? "": "&")
		.append(URLEncoder.encode(entry.getKey(), "UTF-8"))
	        .append("=")
	        .append(URLEncoder.encode(entry.getValue().toString(),"UTF-8"));
	}
	return sb.toString();
    }

    public static Map toMap(Object... items) {
	Map map = new HashMap();
	for (int i = 0; i+1 < items.length; i += 2) 
	    map.put(items[i], items[i+1]);
	return map;
    }

    public static Map copy(Map map) {
	Map cp = new HashMap();
	cp.putAll(map);
	return cp;
    }

    public static Map dissoc(Map map, Object... ks) {
	for (Object k : ks) map.remove(k);
	return map;
    }

    public static Map merge(Map map, Object... kvs) {
	for (int i = 0; i < kvs.length; i += 2) 
	    if (i+1 < kvs.length) map.put(kvs[i], kvs[i+1]);
	return map;
    }

    public static Map<String, Object> textPayload(String text) {
	Map<String, Object> req = new HashMap();
	Map<String, Object> m = new HashMap<String, Object>();
	req.put("document", (Object) m);
	req.put("encodingType", (Object) "UTF8");
	m.put("type", (Object) "PLAIN_TEXT");
	m.put("language", (Object) "EN");
	m.put("content", (Object) text);
	return req;
    }

    public static String toJson(Map<String, Object> map) {
	if (map == null) return "";
	StringBuilder sb = new StringBuilder("{");
	for (Map.Entry<String, Object> entry : map.entrySet()) {
	    sb.append(sb.length() == 1? "": ",")
		.append("\"" + entry.getKey() + "\": ")
		.append(entry.getValue() instanceof Map? 
			toJson((Map<String, Object>) entry.getValue()) :
			"\"" + entry.getValue() + "\"");
	}
	return sb.append("}").toString();
    }

    private static void toStream(OutputStream stream, Map<String, Object> map) 
        throws IOException {
	DataOutputStream out = new DataOutputStream(stream);
	out.writeBytes(toJson(map));
	out.flush();
	out.close();
    }

    private static String toString(InputStream in) throws IOException {
	BufferedReader br = new BufferedReader(new InputStreamReader(in));
	StringBuffer sb = new StringBuffer();
	String inputLine;
	while ((inputLine = br.readLine()) != null)
	    sb.append(inputLine);
	in.close();
	return sb.toString();
    }

}
