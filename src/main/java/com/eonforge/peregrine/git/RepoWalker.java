package com.eonforge.peregrine.git;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.eonforge.peregrine.Config;
import com.eonforge.peregrine.parse.Json;
import com.eonforge.peregrine.http.HClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class RepoWalker {
   
    private static final Logger log = LoggerFactory.getLogger(RepoWalker.class);

    public RepoWalker() { }

    public static void main(String[] args) {
	System.out.println(walk((a,b) -> a + b));
    }

    public static <T> List<T> walk(BiFunction<String, String, T> repofn) {
	return walk(repofn, false);
    }

    public static <T> List<T> walk(BiFunction<String, String, T> repofn,
		    Boolean doAll) {
	// NB - doAll will attempt to process MILLIONS of repositories
	List<T> results = new ArrayList();
	try {
	  Repo repo = new Repo(Config.getProperty("username"),
			       Config.getProperty("password"));
	  Integer page = null, rows = null;
	  Path cachedir = Paths.get("repos.json");
	  String reposStr = Files.exists(cachedir)? 
	          readLines(cachedir):
		  HClient.get(Config.getProperty("repos-url") + 
			  (page==null? "": "&page="+page +
			   (rows==null? "": "&per_page="+rows)), new HashMap());
	  if (!Files.exists(cachedir))
	      Files.write(cachedir, Arrays.asList(reposStr.split("\n")),
			  Charset.defaultCharset());
	  Object reposObj = Json.parse(reposStr);
	  Object repos = Json.get(reposObj, "items"), node = null;
	  long i = 0l, id = 1l;
	  long start = Long.valueOf(Config.getProperty("repos-start", "0"));
	  long end = Long.valueOf(Config.getProperty("repos-end", "1"));
	  long total = (Long) Json.get(reposObj, "total_count");
	  while ((node = Json.get(repos, "" + i)) != null && i++ < end) {
              if (i < start) continue;
	      id = (Long) Json.get(node, "id");
	      log.info("repo count = " + i + " of " + total + ", id=" + id);
	      log.debug("url = " + Json.get(node, "html_url") + 
		       ", name = " + Json.get(node, "name"));
	      results.add(repofn.apply(
	          repo.fetch((String) Json.get(node, "html_url"),
		              (String) Json.get(node, "name")),
		  (String) Json.get(node, "name")));
	  }
          log.info("Done processing " + (i-1) + " repos");
	} catch (Exception e) {
	  System.err.println(e.getMessage());
	  e.printStackTrace();
	}
	log.info("Returning results " + results);
	return results;
    }


    public static String readLines(Path path) {
	StringBuilder sb = new StringBuilder();
	FileReader fr = null;
	BufferedReader br = null;
	try {
	   fr = new FileReader(new File(path.toString()));
	   br = new BufferedReader(fr);
	   String line;
	   while ((line = br.readLine()) != null)
	       sb.append(line).append("\n");
	/*try (Stream<String> lines = Files.readAllLines(path, 
		Charset.defaultCharset()).stream()) {
	    return lines.collect(Collectors.joining("\n"));*/
	} catch (IOException e) {
	    log.error("Error reading file " + path, e);
	} finally {
	    try { fr.close(); } catch (Exception r) { }
	    try { br.close(); } catch (Exception r) { }
	}
	return sb.toString();
    }

    public static void walkPublicRepos(String url, String username, 
		    String password) {
	Repo repo = new Repo(username, password);
	if (url == null) url = "https://api.github.com"; //repositories";
	String response;
	log.info("Fetching url {}", url);
	Map<String, String> params = new HashMap();
	params.put("username", username);
	params.put("password", password);
	try { response = HClient.get(url, null); }
	catch (Exception e) { log.error("Error during GET", e); return; }
	log.info("response = {}", response);
        Object json = Json.parse(response);
	Object node = null;
	int count = 0;
	while ((node = Json.get(json, ++count+"")) != null && count < 1) {
	    log.info("repo = {}, count = {}", repo, count);
	    repo.fetch((String) Json.get(node, "url"), 
		       (String) Json.get(node, "name"));
	}
    }

}
