package com.eonforge.peregrine.git;


import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Properties;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.transport.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eonforge.peregrine.Config;

/**
 * Fetch remote git repos!
 *
 */
public class Repo
{

    private String username, password;
    private static final Logger log = LoggerFactory.getLogger(Repo.class);

    public Repo(String username, String password) {
	this.username = username; this.password = password;
    }

    public String fetch(String remoteUri, String name) {
	
	try {
            CredentialsProvider cp = new UsernamePasswordCredentialsProvider(
			   username, password);

	    Path dirPath = Paths.get(
                Config.getProperty("git-local-dir", "repos"), name);
	    String dir = dirPath.toString();
            if (Files.exists(dirPath)) {
		log.info("Skipping fetch for locally existing repo " + dir);
		return dir;
	    } else {
		Files.createDirectories(dirPath);
	        log.info("Created repos dir " + dirPath.toString());
	    }
	    log.info("Remote uri = {}, local dir = {}", remoteUri, dir);
	    CloneCommand cc = new CloneCommand().setCredentialsProvider(cp)
		.setDirectory(new File(dir))
		.setURI(remoteUri);

	    Git git = cc.call();
            Collection<Ref> remoteRefs = git.lsRemote()
                .setCredentialsProvider(cp)
                .setRemote("origin")
                .setTags(true)
                .setHeads(false)
                .call();
            for (Ref ref : remoteRefs) {
                log.info(ref.getName() + " -> " + ref.getObjectId().name());
            }    
	    return dir;
	} catch (Exception e) {
	    log.error(e.getMessage());
	    e.printStackTrace();
	}
	return null; 
    }
}
