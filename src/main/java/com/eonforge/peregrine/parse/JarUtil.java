package com.eonforge.peregrine.parse;

import java.util.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.util.Enumeration;
import java.util.function.Function;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;


public class JarUtil {

    private static final String SEP = FileSystems.getDefault().getSeparator();

    public JarUtil() { }


    public static void extract(String jarPath, String destination, 
        Function<String, Boolean> entryFn) throws IOException {
         try (JarFile jarFile = new JarFile(jarPath)){
             File dir = new File(destination);
             dir.mkdir();
             for (Enumeration e = jarFile.entries(); e.hasMoreElements();) {
                 JarEntry entry = (JarEntry) e.nextElement();
                 if(entry.isDirectory()) {
                     File childFolder = new File(dir, entry.getName());
                     childFolder.mkdir();
                 } else {
                     try(FileOutputStream fos = new FileOutputStream(
                          dir.getAbsolutePath() + SEP + entry.getName())) {
                         InputStream zis = jarFile.getInputStream(entry);
                         byte[] data = new byte[1024];
 
                         int bytesRead = zis.read(data);
                         while(bytesRead != -1) {
                             fos.write(data,0,bytesRead);
                             bytesRead = zis.read(data);
                         }
                         fos.flush();
                         fos.close();
                     }
                     entryFn.apply(dir.getAbsolutePath()+SEP+entry.getName());
                 }
             }
         }
    }

}
