package com.eonforge.peregrine.parse;

import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Json {
    
    public static Object get(Object json, String... keys) {
	if (keys.length == 0 || json == null) return json;
	String[] t = Arrays.copyOfRange(keys, 1, keys.length);
	if (json instanceof JSONArray) {
	    if (Integer.valueOf(keys[0]) >= ((JSONArray) json).size())
		return null;
	    return get(((JSONArray) json).get(Integer.valueOf(keys[0])), t);
	}
	return get(((JSONObject) json).get(keys[0]), t);
    }

    public static Object put(Object json, Object value, String... keys) {
	if (keys.length <= 0) return json;
	else if (keys.length == 1) {
	    if (json instanceof JSONArray)
		((JSONArray) json).set(Integer.valueOf(keys[0]), value);
	    else ((JSONObject) json).put(keys[0], value);
	} else {
	    if (get(json, keys[0]) == null) 
		put(json, new JSONObject(), keys[0]);
	    put(get(json, keys[0]), value, Arrays.copyOfRange(keys,1,keys.length));
	}
	return json;
    }

    public static <T> T[] copyRange(T[] vs, int start, int end) {
	List<T> t = new ArrayList<T>();
	for (int i = start; i < end; i++) t.add(vs[i]);
	return t.toArray(vs);
    }

    public static Object parse(String json) {
	try {
	    return (new JSONParser()).parse(json);
	} catch (Exception e) {
	    System.err.println(e.getMessage());
	    e.printStackTrace();
	}
	return null;
    }

    public static String toJsonString(Object obj) {
	Object json = toJson(obj);
	if (json instanceof JSONArray) return ((JSONArray)json).toJSONString();
        return ((JSONObject) json).toJSONString();
    }

    public static Object toJson(Object obj) {
	Object json = obj;
	if (obj instanceof Map) {
	    json = new JSONObject();
	    for (String key : ((Map<String, Object>) obj).keySet()) {
		((JSONObject)json).put(key,
			toJson(((Map<String,Object>)obj).get(key)));
	    }
	} else if (obj instanceof Collection || obj instanceof Object[]) {
	    json = new JSONArray();
	    for (Object item : (Object[]) (obj instanceof Object[]? obj:
			((Collection) obj).toArray()))
		((JSONArray)json).add(toJson(item));
	} else if (obj instanceof Iterable) {
	    Iterator iter = ((Iterable) obj).iterator();
	    json = null;
	    while (iter.hasNext()) {
		Object next = iter.next();
		if (next instanceof Map.Entry) {
		    if (json == null) json = new JSONObject();
		    ((JSONObject) json).put(toJson(((Map.Entry)next).getKey()),
			   toJson(((Map.Entry)next).getValue()));
		} else {
		    if (json == null) json = new JSONArray();
		    ((JSONArray) json).add(toJson(next));
		}
	    }
	}
	return json;
    }

    public static Object fromJson(Object json) {
        if (json instanceof JSONObject) {
	    return ((Map)json).entrySet().stream().map(e -> new Object[]{
		((Entry)e).getKey(), fromJson(((Entry)e).getValue())}).collect(
		Collectors.toMap(e -> ((Object[])e)[0], e -> ((Object[])e)[1]));
	} else if (json instanceof JSONArray) {
	    return ((List)json).stream().map(Json::fromJson).collect(
	        Collectors.toList());
	}/* else if (json instanceof JSONNumber) {
	    return ((JSONNumber) json).doubleValue();
	} else if (json instanceof JSONString) {
	    return ((JSONString) json).getString();
	} else if (json == JSONValue.TRUE) {
	    return Boolean.TRUE;
	} else if (json == JSONValue.FALSE) {
	    return Boolean.FALSE;
	}*/
	return json;
    }
}
