package com.eonforge.peregrine.parse;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import org.w3c.dom.traversal.DocumentTraversal;
import org.w3c.dom.traversal.NodeFilter;
import org.w3c.dom.traversal.NodeIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Xml {

    private static final Logger log = LoggerFactory.getLogger(Xml.class);

    public Document rootDoc = null;
    private Object curr = null;

    private Xml(Document rootDoc) { this.rootDoc = rootDoc; }

    public static Xml parse(String path) throws 
        ParserConfigurationException, SAXException, IOException {
	DocumentBuilderFactory dbFactory = 
            DocumentBuilderFactory.newInstance();
	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	Document doc = dBuilder.parse(new File(path));

	doc.getDocumentElement().normalize();
        return new Xml(doc); 
    }

    public static String render(Object obj) throws TransformerException,
        TransformerConfigurationException {
        if (obj instanceof Xml)
            return render((Node)((Xml)obj).rootDoc);
        else if (obj instanceof NodeList) {
            NodeList list = (NodeList) obj;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < list.getLength(); i++)
                sb.append(render(list.item(i)));
            return sb.toString().replaceAll("<\\??xml[^>]+>","");
        }
        return render((Node) obj);
    }

    public static String render(Node node) throws TransformerException,
        TransformerConfigurationException {
        Transformer transform = TransformerFactory.newInstance().
            newTransformer();
        transform.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transform.setOutputProperty(OutputKeys.INDENT, "yes");
        Writer out = new StringWriter();
        transform.transform(new DOMSource(node), 
            new StreamResult(out));
        return out.toString();
    }

    public static int pos(Object o) {
        String a = (o == null) ? "" : o.toString();
        return !a.contains(":") ? -1 : 
            Integer.valueOf(a.substring(0, a.indexOf(":")));
    }

    public static String render(Map map) {
        StringBuilder sb = new StringBuilder();
        List keys = new ArrayList(map.keySet());
        keys.sort(new Comparator() {
            public int compare(Object a, Object b) { return pos(a) - pos(b); }
        });
        boolean found = false;
        for (int i = 0; i < keys.size(); i++) 
            found |= pos(keys.get(i)) >= 0;
        for (Object k : keys) {
            if (found && pos(k) < 0) continue;
            String t = !k.toString().contains(":") ? k.toString() :
                k.toString().substring(k.toString().indexOf(":")+1);
            sb.append("<" + t + ">").append(map.get(k)).
               append(Pattern.matches(".*-?[\\/\\?\\-]$", t) ? "" :
                      "</" + t.split("\\s+")[0] + ">");
        }
        return sb.toString();
    }

    public static int toInt(Object o) {
        try { return Integer.valueOf(String.valueOf(o)); }
        catch (Exception e) { return 0; }
    }

    public static Object get(Object doc, String... path) {
        if (path.length == 0) return doc;
        String k = path[0]; 
        String[] rest = new String[path.length-1];
        log.trace("Xml.get.k = "+k+", rest="+Arrays.asList(rest)+", doc="+doc);
        for (int i = 1; i < path.length; i++) rest[i-1] = path[i];
        if (doc instanceof Document)
            return get(((Document) doc).getElementsByTagName(k), rest);
        else if (doc instanceof NodeList && Pattern.matches("^[0-9]+$", k))
            return get(((NodeList) doc).item(toInt(k)), rest);
        else if (doc instanceof Node && ((Node) doc).getNodeType() ==
                Node.ELEMENT_NODE) 
            return get(((Element) doc).getElementsByTagName(k), rest);
        else if (doc instanceof NamedNodeMap) {
            if (Pattern.matches("[\\+\\-]?[0-9\\.]+", k))
                return get(((NamedNodeMap) doc).item(Integer.valueOf(k)), rest);
            return get(((NamedNodeMap) doc).getNamedItem(k), rest);
        }
        return null;
    }

    public static String getAsXml(Object doc, String k) {
        return "<" + k + ">" + Xml.get(doc, k) + "</" + k + ">";
    }

    public static Map<String, String> getAsMap(Object doc, String k)
        throws TransformerException, TransformerConfigurationException {
        Object el = get(doc, k);
        return getAsMap(render(el), k);
    }

    public static Map<String, String> getAsMap(String xml, String k) {
        return getAsMap(xml, k, false);
    }

    public static Map<String, String> getAsMap(String xml, String k, 
        boolean keepOrder) {
        Map<String, String> map = new HashMap();
        xml = removeComments(xml);
        Matcher m = Pattern.compile("<([^>]+)>").matcher(xml);
        while (m.find()) {
            String tag = m.group(1);
            log.trace("getAsMap().tag = " + tag + ", k = " + k);
            if (k != null && tag.equals(k)) continue;
            String[] toks = tag.split("\\s+");
            String tname = toks.length == 0 ? tag :
                (toks[0].length()>0 ? toks[0] : (toks.length>1? toks[1]: ""));
            log.trace("getAsMap().tname = " + tname + ", xml = " + xml);
            String updated = updateMap(xml, map, tag, tname, keepOrder);
            if (!updated.equals(xml)) {
                xml = updated;
                m = Pattern.compile("<([^>]+)>").matcher(xml);
            }
        }
        return map;
    }

    private static String updateMap(String xml, Map<String, String> map,
        String tag, String tagName, boolean keepOrder) {
        tagName = tagName.replaceAll("^/+", "");
        tag = tagName;
        Matcher m1 = Pattern.compile(".*(</\\s*" + tagName + "\\s*>).*")
                            .matcher(xml);
        String val = "";
        if (m1.find()) {
            String ctag = m1.group(1); 
            int bpos = xml.indexOf("<"+tag+">")+tag.length()+2;
            bpos = bpos < 0 ? 0 : (bpos >= xml.length()? xml.length()-1 : bpos);
            int end = xml.indexOf(ctag);
            String body = xml.substring(bpos, end > bpos ? end : xml.length());
            Integer[] ids = occurrences(body, "<"+tag+">");
            int cpos = occurrences(xml, ctag)[ids.length];
            val = xml.substring(bpos, cpos > bpos ? cpos : xml.length());
            xml = xml.substring(cpos > bpos ? cpos+ctag.length(): xml.length());
            log.trace("updateMap().val = " + val + ", xml = " + xml);
        } else if (xml.contains("<"+tag+">"))
            xml = xml.substring(xml.indexOf("<"+tag+">")+tag.length()+2);
        map.put(keepOrder ? map.size() + ":" + tag : tag, val);
        return xml;
    }

    public static Integer[] occurrences(String str, String tok) {
        List<Integer> pos = new ArrayList();
        for (int i = 0; i <= str.length() - tok.length(); i++) {
            if (str.substring(i, i+tok.length()).equals(tok))
                pos.add(i);
        }
        return pos.toArray(new Integer[0]);
    }

    public static String removeComments(String xml) {
        return xml.replaceAll("\n", "<mybr>").
                   replaceAll("<!--(.(?!-->))*.-->", "").
                   replaceAll("<mybr>", "\n");
    }

    public static List<Map<String, String>> getAsMaps(Object doc, String k)
        throws TransformerException, TransformerConfigurationException {
        return getAsMaps(doc, k, "");
    }

    public static List<Map<String, String>> getAsMaps(Object doc, String k,
        String subK) 
        throws TransformerException, TransformerConfigurationException {
        List<Map<String, String>> maps = new ArrayList();
        String xml = render(get(doc, k));
        log.trace("getAsMaps.xml = " + xml);
        for (String tok : xml.split("<" + k + ">")) {
            if (tok.trim().length() == 0) continue;
            log.trace("getAsMaps.tok = " + tok + ", k = " + k);
            String ex = "";
            if (tok.contains("<" + subK + ">")) 
                ex = tok.substring(tok.indexOf("<" + subK + ">"), 
                     tok.indexOf("</"+subK+">") + ("</"+subK+">").length());
            //tok = tok.substring(0,tok.indexOf("</" + k + ">")).replace(ex,"");
            tok = tok.replace(ex, "");
            maps.add(getAsMap("<" + k + ">" + tok, k));
            if (ex.length() > 0) 
                maps.get(maps.size()-1).put(subK,ex.trim().replaceAll("\n",""));
        }
        return maps;
    }

    public void set(Object doc, String value, String... path) {
        if (path.length == 0) { createTextNode(doc, value); return; }
        String k = path[0]; 
        List<String> rest = Arrays.asList(path).subList(1, path.length);
        Object child = null;
        log.trace("set: k = " + k + ", rest = " + rest + ", path = " + 
                 Arrays.asList(path) + ", doc = " + doc);
        if (Pattern.matches("^[0-9]+$", k)) {
            for (int i = length(doc); i <= Integer.valueOf(k); i++)
                child = createElement(doc, rest.get(0));
            String nextk = rest.isEmpty()? k: rest.get(0);
            if (child != null) rest = rest.subList(1, rest.size());
            else child = ((NodeList) doc).item(Integer.valueOf(k));
            log.trace("set: isNum k = " + k + ", child = " + child + 
                     ", rest = " + rest + ", length = " + length(doc));
            k = nextk; 
        } else if (doc instanceof Node && ((Node) doc).getNodeType() ==
                Node.ELEMENT_NODE) {
            child = get(((Element) doc).getElementsByTagName(k), k);
        } else if (doc instanceof NamedNodeMap) {
            if (Pattern.matches("[\\+\\-]?[0-9\\.]+", k))
                child = ((NamedNodeMap) doc).item(Integer.valueOf(k));
            else child = ((NamedNodeMap) doc).getNamedItem(k);
        } else if (doc instanceof Document) {
            child = get(((Document) doc).getElementsByTagName(k), k);
        } else if (doc instanceof NodeList)
            child = get((NodeList) doc, k);
        log.trace("set: doc = " + doc + ", child = " + child + ", k = " + k);
        if (child instanceof NodeList && length(child) == 0 && 
	    !Pattern.matches("^[0-9]+$", k) || child == null)
            child = createElement(doc, k);
        set(child, value, rest.toArray(new String[0])); //NPE child ok, for now
    }     

    private Node get(NodeList node, String key) {
        List<Node> nodes = search(node, key);
        return nodes.isEmpty()? null: nodes.get(0);
    }

    public static List<Node> getChildren(Object root, String key) {
        List<Node> children = new ArrayList();
        Document document = (Document) root;
        DocumentTraversal trav = (DocumentTraversal) document;
        NodeIterator it = trav.createNodeIterator(document.getDocumentElement(),
            NodeFilter.SHOW_ELEMENT, null, true);
        for (Node node = it.nextNode(); node != null;
                node = it.nextNode()) {
            String name = node.getNodeName();
            log.trace("getChildren().name = " + name);
            if (name.trim().equals(key.trim()))
                children.add(node);
        }
        return children;
    }

    public static List<Node> search(NodeList node, String key) {
        List<Node> nodes = new ArrayList();
        log.trace("get: length = " + node.getLength());
        for (int i = 0; i < node.getLength(); i++) {
            log.trace("get: i = " +i+ ", item = " + node.item(i) + ", name = "+
                (node.item(i)==null? "": node.item(i).getNodeName()) + 
                ", key = " + key);
            if (node.item(i) != null && key.equals(node.item(i).getNodeName()))
                nodes.add(node.item(i));
        }
        return nodes;
    }

    private Object createElement(Object doc, String k) {
        return appendChild(doc, rootDoc.createElement(k));
    }

    private Object createTextNode(Object doc, String k) {
        return appendChild(doc, rootDoc.createTextNode(k));
    }
 
    private static Object appendChild(Object doc, Node child) {
        log.trace("appendChild: doc = " + doc + ", child = " + child + 
	    ", doc.type = " + Arrays.asList(doc instanceof NamedNodeMap,
	    doc instanceof Element, doc instanceof Document,
	    doc instanceof Node, doc instanceof NodeList));
        if (doc instanceof NamedNodeMap)   
            return ((NamedNodeMap) doc).setNamedItem(child);
        else if (doc instanceof Element) 
            return ((Element) doc).appendChild(child);
        else if (doc instanceof Document)
            return child;
        else if (doc instanceof Node && ((Node) doc).getNodeType() ==
                Node.ELEMENT_NODE) 
            return ((Element) doc).appendChild(child);
        else if (doc instanceof NodeList)
            return getNearest(doc).appendChild(child); //TODO: redo properly
        return child;
    }

    private static Document getDocument(Object doc) {
        log.trace("getDocument: nearest = " + getNearest(doc) + ", doc=" + doc);
        log.trace("getDocument: parent = " + getNearest(doc).getParentNode());
        return getNearest(doc).getOwnerDocument(); //TODO: throw NPE if bad doc
    }

    private static Node getNearest(Object doc) {
        if (doc instanceof NamedNodeMap)   
            return ((NamedNodeMap) doc).item(0);
        else if (doc instanceof Element) 
            return ((Element) doc);
        else if (doc instanceof Document)
            return ((Document) doc);
        else if (doc instanceof Node && ((Node) doc).getNodeType() ==
                Node.ELEMENT_NODE) 
            return ((Element) doc);
        else if (doc instanceof NodeList)
            return ((NodeList) doc).item(0);
        return null;

    }

    private static String tag(Object node) {
        if (node instanceof Node) {
            Node n = (Node) node;
            return n.getNodeName() + "-" + n.getNodeValue();
        } else if (node instanceof Element)
            return ((Element) node).getTagName();
        return "";
    }

    private static int length(Object doc) {
        int len = 0;
        NodeList nodes = toNodeList(doc);
        if (nodes != null)
            for (int i = 0; i < nodes.getLength(); i++) 
                if (nodes.item(i) != null && 
                    !"#text-".equals(tag(nodes.item(i)).trim()))
                    len++;
        return len;
    }

    private static NodeList toNodeList(Object doc) {
        if (doc instanceof NodeList) 
            return ((NodeList) doc);
        else if (doc instanceof Node)
            return ((Node) doc).getChildNodes();
        return null;
    }
 
}
