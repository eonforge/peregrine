package com.eonforge.peregrine;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.FileSystems;
import java.util.Collection;
import java.util.Base64;
import java.util.Properties;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.transport.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eonforge.peregrine.git.RepoWalker;

/**
 * Fetch remote git repos!
 *
 */
public class Config 
{

    public static Properties properties = new Properties();
    public static boolean isLoaded = false;
    public static String SEP = FileSystems.getDefault().getSeparator();
    private static final Logger log = LoggerFactory.getLogger(Config.class);

    public static void load(String file) {
	try {
	    InputStream st = inputStream(file);
            if (st != null) 
	        properties.load(st);
	    log.info("Loaded " + file);
	    isLoaded = true;
	} catch (Exception e) {
	    System.err.println(e.getMessage());
	    e.printStackTrace();
	}
    }

    private static InputStream inputStream(String file) 
	throws FileNotFoundException {
        String sysConf = System.getProperty("spring.config.name");
	file = sysConf != null && sysConf.trim().length() > 0 ? sysConf : file;
	log.info("systemFile().sysConf = " + sysConf + ", file = " + file);
	log.info("inputStream.isFile = " + (new File(file)).isFile());
	if ((new File(file)).isFile())
	    return new FileInputStream(file);
	else return Config.class.getClassLoader().getResourceAsStream(file);
    }

    public static boolean contains(String name) {
	return getProperty(name) != null;
    }

    public static String getPwd() {
        try {
            return Config.class.getProtectionDomain().getCodeSource().
                getLocation().getPath().toString().replace("file:", "").
                replaceAll("[^\\\\/]*!.*", "");
        } catch (Exception e) {
            log.error("Error fetching pwd " + e, e);
        }
        return "";
    }
  
    public static String getProperty(String name) {
	log.trace("Request to get property " + name);
	if (!isLoaded) {
	    load("application.properties");
	}
	return properties.getProperty(name);
    }

    public static String getPathProperty(String name) {
        return getPathProperty(name, "");
    }

    public static String getPathProperty(String name, String defaultVal) {
        String v = Config.getProperty(name);
        return v==null ? defaultVal : v + (v.endsWith(SEP)? "": SEP);
    }

    public static String getProperty(String name, String defaultVal) {
	String val = getProperty(name);
	return val == null? defaultVal: val;
    }

    public static void setProperty(String name, String value) {
	if (!isLoaded) {
	    log.info("Loading properties file request to set " + name);
	    load("babel.properties");
	}
	properties.setProperty(name, value);
    }

    public static int getPropertyInt(String name) {
	return getPropertyInt(name, 0);
    }

    public static int getPropertyInt(String name, int defaultVal) {
	return contains(name)? Integer.valueOf(getProperty(name)): defaultVal;
    }

    public static void main( String[] args ) {

	String user = getProperty("username");
	String password = getProperty("password");
	if (user == null || password == null) {
	    try {
	    //String pass = encrypt("myuser", "mypass"); 
	    //System.out.println("pass = " + pass);
	    //System.out.println("decrypted = " + decrypt(pass));
	    } catch (Exception e) { e.printStackTrace(); }
	    user = "someuser"; password = "somepassword";
	    //System.out.println("console = " + System.console());
            //user = new String(System.console().readLine("Username: "));
            //password = new String(System.console().readPassword("Pass: "));
	}

	//Repo repo = new Repo(user, password);
	//repo.fetch(getProperty("git-test-uri"), getProperty(
	//			    "git-test-reponame"));
	RepoWalker.walkPublicRepos(null, user, password);
    }

     private static String encrypt(String username, String password)
	     throws Exception {
        byte[] keyData = (username + password).getBytes();
        SecretKeySpec secretKeySpec = new SecretKeySpec(keyData, "Blowfish");
        Cipher cipher = Cipher.getInstance("Blowfish");
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
        byte[] bytes = cipher.doFinal(password.getBytes());
        return Base64.getEncoder().encodeToString(bytes);
    }
     
    private static String decrypt(String string) throws Exception {
        byte[] keyData = string.getBytes();
        SecretKeySpec secretKeySpec = new SecretKeySpec(keyData, "Blowfish");
        Cipher cipher = Cipher.getInstance("Blowfish");
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
        byte[] bytes = cipher.doFinal(Base64.getDecoder().decode(string));
        return new String(bytes);
    }
}
