#Peregrine - Code artefact search and retrieval module 

The [Peregrine Falcon](https://en.wikipedia.org/wiki/Peregrine_falcon#Relationship_with_humans)  also known as the peregrine, and historically as the duck hawk in North America, is a widespread bird of prey in the family Falconidae. The peregrine falcon is a highly admired falconry bird, and has been used in falconry for more than 3,000 years, beginning with nomads in central Asia.

##Compiling and Running

This is a maven project, so the usual maven targets apply:
```
mvn clean package [test]
```

##Updating maven repository

For development purposes, updating snaphots is necessary so changes are picked up by dependent projects. 
```
mvn clean install -U
or
mvn dependency:purge-local-repository clean package
```